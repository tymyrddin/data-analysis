# Credit card defaulters

Analyse the characteristics of customers who are most likely to default on their credit card payments using univariate and bivariate analysis techniques on the [Default of credit card clients data set](https://archive.ics.uci.edu/ml/datasets/default+of+credit+card+clients).

- [x] [Preprocessing](preprocessing.ipynb)
- [x] [Exploratory Data Analysis](eda.ipynb)
- [ ] [Evaluating the correlation between columns using a heatmap](heatmap.ipynb)