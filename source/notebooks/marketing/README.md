# Analysing (bank) marketing campaign data

Modeling the relationships between the different features in the [Bank Marketing dataset](https://archive.ics.uci.edu/ml/datasets/Bank+Marketing) and their impact on the final outcome of the campaign., and applying linear and logistic regression models for analysing the outcome of a marketing campaign.

- [x] [Initial data analysis](initial.ipynb)
- [x] [Impact of numerical features on the outcome](numerical_impact.ipynb)
- [ ] [Regressions](regressions.ipynb)
