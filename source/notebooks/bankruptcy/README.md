# Company bankruptcy

Exploratory data analysis using pandas profiling on the [Polish' companies bankruptcy dataset](https://archive.ics.uci.edu/ml/datasets/Polish+companies+bankruptcy+data), applying missing value treatments, and successfully handle imbalances in the data, to try and understand the main reasons behind bankruptcy and whether it could be possible to identify early warning signs.

- [x] [Loading arff data](loading_data.ipynb)
- [x] [Profiling](profiling.ipynb)
- [x] [Missing values](missing.ipynb)
- [x] [Feature selection](features.ipynb)