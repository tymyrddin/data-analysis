# Online shopper's purchasing intention

Applying univariate and bivariate analysis on the [Online Shoppers Purchasing Intention dataset](https://archive.ics.uci.edu/ml/datasets/Online+Shoppers+Purchasing+Intention+Dataset), and applying clustering and making recommendations based on the predictions.

- [x] [Preprocessing](preprocessing.ipynb)
- [x] [Exploratory data analysis](eda.ipynb)
- [x] [Clustering](clustering.ipynb)
