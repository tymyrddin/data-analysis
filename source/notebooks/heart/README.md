# Heart disease

Searching for missing values and outliers, plotting visualizations to observe trends and patterns that exist in the [Heart Disease dataset](https://archive.ics.uci.edu/ml/datasets/Heart+Disease), and creating visualizations to understand how the features affect the target column of the dataset.

- [x] [Preprocessing](preprocessing.ipynb)
- [x] [Outliers](outliers.ipynb)
- [x] [Exploratory data analysis](eda.ipynb)
- [x] [Correlations with heatmap](heatmap.ipynb)