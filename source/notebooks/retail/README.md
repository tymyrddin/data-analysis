# Online retail

Searching for and dealing with missing values, outliers, and anomalies within the [Online Retail II dataset](https://archive.ics.uci.edu/ml/datasets/Online+Retail+II).

- [x] [Preprocessing](preprocessing.ipynb)
- [x] [Outliers and missing values](missing.ipynb)
- [x] [Data preparation and feature engineering](features.ipynb)
- [x] [Data analysis](analysis.ipynb)
