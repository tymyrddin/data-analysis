# Bike sharing

Applying visual analysis, hypothesis testing, and time series analysis to the [Bike Sharing Dataset](https://archive.ics.uci.edu/ml/datasets/Bike+Sharing+Dataset#).

- [x] [Preprocessing](preprocessing.ipynb)
- [x] [Hypothesis testing](hypothesis_testing.ipynb)
- [x] [Analysis of weather related features](weather.ipynb)
- [x] [Time-series analysis](time_series.ipynb)
- [x] [ARIMA models](arima.ipynb)