import pytest
from testbook import testbook

notebook = "/absenteism/preprocessing.ipynb"


# Set up a shared notebook context to speed up tests.
@pytest.fixture(scope="module")
def tb():
    with testbook(notebook, execute=True) as tb:
        yield tb
