# Absenteism at work

Applying estimating conditional probabilities, Bayes' theorem, and Kolmogorov-Smirnov tests, for distribution comparison, and data transformation techniques, such as the Box-Cox and Yeo-Johnson transformations, on the [Absenteism at work dataset](https://archive.ics.uci.edu/ml/datasets/Absenteeism+at+work)

- [x] [Preprocessing](preprocessing.ipynb)
- [x] [Initial analysis of reasons for absence](reasons.ipynb)
- [x] [Conditional probabilities of the different absence reasons](probabilities.ipynb)
- [x] [Impact of Body Mass Index on reason for absence](bmi.ipynb)
- [x] [Impact of age on reason for absence](age.ipynb)
- [x] [Impact of education on reason for absence](education.ipynb)
- [x] [Transportation costs and distance to work factors](distance.ipynb)
- [x] [Temporal factors](temporal.ipynb)