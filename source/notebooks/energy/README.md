# Energy consumed by appliances

Analysing individual features of the [Appliances energy prediction dataset](https://archive.ics.uci.edu/ml/datasets/Appliances+energy+prediction) to assess whether the data is skewed.

- [x] [Preprocessing](preprocessing.ipynb)
- [x] [Outliers and missing](missing.ipynb)
- [x] [Data preparation and feature Engineering](features.ipynb)
- [x] [Data analysis](eda.ipynb)
