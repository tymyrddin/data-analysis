# Analysing air quality

Dealing with missing values, feature engineering, exploratory data analysis, design visualizations, and summarizing insights provided by the [Beijing Multi-Site Air-Quality dataset](https://archive.ics.uci.edu/ml/datasets/Beijing+Multi-Site+Air-Quality+Data).

- [x] [Preprocessing](preprocessing.ipynb)
- [x] [Outliers and missing](missing.ipynb)
- [x] [Data analysis](eda.ipynb)
